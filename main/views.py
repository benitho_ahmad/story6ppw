from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import KegiatanForm, PesertaForms
from .models import Kegiatan, Peserta

def tambahKegiatan(request):
    context = {'form' : KegiatanForm}
    return render(request, "main/tambahKegiatan.html", context)

def saveKegiatan(request):
    form = KegiatanForm(request.GET or None)
    if(form.is_valid and request.method == "GET"):
        form.save()
        return HttpResponseRedirect('/hasil')

def tambahPeserta(request):
    context = {'form' : PesertaForms}
    return render(request, "main/tambahPeserta.html", context)

def savePeserta(request):
    form = PesertaForms(request.GET or None)
    if(form.is_valid and request.method == "GET"):
        form.save()
        return HttpResponseRedirect('/hasil')

def hasil(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    re = {'peserta':peserta, 'list_kegiatan':kegiatan}
    return render(request, "main/hasil.html", re)


