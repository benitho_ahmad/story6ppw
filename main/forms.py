from django import forms
from .models import Kegiatan, Peserta

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['Nama_Kegiatan']
    Nama_Kegiatan = forms.CharField(max_length=50)

class PesertaForms(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['Nama_Peserta', 'kegiatan']
    Nama_Peserta = forms.CharField(max_length=50)
    kegiatan = forms.ModelMultipleChoiceField(queryset = Kegiatan.objects.all())