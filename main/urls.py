from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('tambahKegiatan/', views.tambahKegiatan, name='tambahKegiatan'),
    path('tambahPeserta/', views.tambahPeserta, name='tambahPeserta'),
    path('tambahKegiatan/saveKegiatan', views.saveKegiatan, name='saveKegiatan'),
    path('tambahPeserta/savePeserta', views.saveKegiatan, name='savePeserta'),
    path('hasil/', views.hasil, name='hasil')
]
