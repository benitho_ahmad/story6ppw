from django.test import LiveServerTestCase, TestCase, Client, tag
from django.urls import reverse
from selenium import webdriver
from .models import Kegiatan, Peserta

class Testing(TestCase):
    def test_cek_hasil(self):
        response = Client().get('/hasil/')
        self.assertEquals(response.status_code, 200)
    
    def test_cek_hasil_template(self):
        response = Client().get('/hasil/')
        self.assertTemplateUsed(response, 'main/hasil.html')

    def test_cek_tambahKegiatan(self):
        response = Client().get('/tambahKegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_cek_tambahKegiatan_template(self):
        response = Client().get('/tambahKegiatan/')
        self.assertTemplateUsed(response, 'main/tambahKegiatan.html')
    
    def test_cek_tambahPeserta(self):
        response = Client().get('/tambahPeserta/')
        self.assertEquals(response.status_code, 200)
    
    def test_cek_tambahPeserta_template(self):
        response = Client().get('/tambahPeserta/')
        self.assertTemplateUsed(response, 'main/tambahPeserta.html')

    def test_home_isi_content(self):
        response = Client().get('/hasil/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Nama Orang", html_kembalian)
        self.assertIn("Kegiatan", html_kembalian)

    def test_tambahKegaiatan_isi_content(self):
        response = Client().get('/tambahKegiatan/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Nama kegiatan:", html_kembalian)

    def test_tambahPeserta_isi_content(self):
        response = Client().get('/tambahPeserta/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Nama peserta:", html_kembalian)
        self.assertIn("Kegiatan:", html_kembalian)

    def test_model_Kegiatan(self):
        Kegiatan.objects.create(Nama_Kegiatan = 'Tidur')
        check_masuk = Kegiatan.objects.all().count()
        self.assertEquals(check_masuk, 1)