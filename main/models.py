from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    Nama_Kegiatan = models.CharField(max_length=50)
    
    def __str__(self):
        return self.Nama_Kegiatan

class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    Nama_Peserta = models.CharField(max_length=50)
    def __str__(self):
        return self.Nama_Peserta